#!/bin/bash

TAG_VERSION=$1

if [[ -z "${TAG_VERSION}" ]]; then
  echo "Empty Tag Version"
elif [[ ! "$TAG_VERSION" =~ ^v[0-9]+.[0-9]+.[0-9]+$ ]]; then
  echo "Invalid Tag Version. Pattern need to follow vX.Y.Z"
  exit
fi

git tag "${TAG_VERSION}"
git push origin "${TAG_VERSION}"
