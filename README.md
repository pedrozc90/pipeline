# Pipeline

Repository to learn about GitLab CI/CD.

-   [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html).

## License

Please, see [LICENSE](./LINCESE) file.
